'use strict';

var bowerFiles = require('main-bower-files');
var concat = require('gulp-concat');
var del = require('del');
var gulp = require('gulp');
var inject = require('gulp-inject');
var minifyCss = require('gulp-clean-css');
var ngAnnotate = require('gulp-ng-annotate');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');

var cssFilesToInject = [
  'www/css/**/*.css'
];
var cssMinFilesToInject = [
  'www/css/**/*.min.css'
];
var jsFilesToInject = [
  'www/lib/ionic.js',
  'www/lib/angular.js',
  'www/lib/**/*.js',
  'www/app/module.js',
  'www/app/**/module.js',
  '!www/app/**/controller.js',
  'www/app/**/*.js'
];
var jsMinFilesToInject = [
  'www/js/**/*.min.js'
];

gulp.task('compile', function onBuild(done) {
  runSequence(
    'clean',
    ['bower', 'sass'],
    'inject', 'watch', done
  );
});

gulp.task('compile:prod', function onBuildProd(done) {
  runSequence(
    'clean', 'bower',
    'sass:prod', 'js:prod', 'inject:prod', done
  );
});

gulp.task('bower', function onBower(done) {
  gulp.src(bowerFiles())
    .pipe(gulp.dest('www/lib/'))
    .on('end', done);
});

gulp.task('clean', function onClean(done) {
  del.sync([
    'www/css',
    'www/js',
    'www/lib'
  ]);
  done();
});

gulp.task('inject', function onInject(done) {
  gulp.src('www/index.html')
    .pipe(inject(gulp.src(cssFilesToInject, { read: false }), { relative: true, name: 'style', empty: true }))
    .pipe(inject(gulp.src(jsFilesToInject, { read: false }), { relative: true, name: 'script', empty: true }))
    .pipe(gulp.dest('www'))
    .on('end', done);
});

gulp.task('inject:prod', function onInjectProd(done) {
  gulp.src('www/index.html')
    .pipe(inject(gulp.src(cssMinFilesToInject, { read: false }), { relative: true, name: 'style', empty: true }))
    .pipe(inject(gulp.src(jsMinFilesToInject, { read: false }), { relative: true, name: 'script', empty: true }))
    .pipe(gulp.dest('www'))
    .on('end', done);
});

gulp.task('js:prod', function onJsProd(done) {
  gulp.src(jsFilesToInject)
    .pipe(concat('app.min.js'))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(gulp.dest('www/js/'))
    .on('end', done);
});

gulp.task('sass', function onSass(done) {
  gulp.src('scss/app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('www/css/'))
    .on('end', done);
});

gulp.task('sass:prod', function onSassProd(done) {
  gulp.src('scss/app.scss')
    .pipe(sass())
    .pipe(minifyCss({ specialComments: 0, processImport: 0 }))
    .pipe(rename('app.min.css'))
    .pipe(gulp.dest('www/css/'))
    .on('end', done);
});

gulp.task('watch', function onWatch() {
  gulp.watch(['scss/**/*.scss'], ['sass']);
  gulp.watch(jsFilesToInject, ['inject'])
    .on('error', function onError(error) {
      if (/enoent/i.test(error.code));
    });
});

gulp.task('default', ['compile']);
gulp.task('build:before', ['compile:prod']);
gulp.task('run:before', ['compile']);
gulp.task('serve:before', ['compile']);
