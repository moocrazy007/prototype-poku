(function _AppServiceGraphqlFactory_() {
  'use strict';

  function graphql() {
    var config = { url: '/graphql' };

    this.set = function set(userConfig) {
      angular.merge(config, userConfig);
    };

    this.$get = function get($q, $http) {
      return function send(query, variables) {
        return $q(function graphqlRequest(resolve, reject) {
          $http({
            method: 'POST',
            url: config.url,
            data: {
              query: query,
              variables: variables
            }
          }).then(function success(response) {
            if (response.errors) throw response.errors;
            resolve(response.data && response.data.data);
          }).catch(reject);
        });
      };
    };
  }

  angular.module('app.services.graphql').provider('graphql', graphql);
}());
