(function _AppServicesGraphqlModule_() {
  'use strict';

  var modules = [];

  angular.module('app.services.graphql', modules);
}());
