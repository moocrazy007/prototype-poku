(function _AppServicesModule_() {
  'use strict';

  var modules = [
    'app.services.graphql'
  ];

  angular.module('app.services', modules);
}());
