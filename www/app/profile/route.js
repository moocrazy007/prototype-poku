(function _AppProfileRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.profile', {
      url: '/profile',
      abstract: true,
      views: {
        'menuContent@app': {
          template: '<ion-nav-view name="menuContent"></ion-nav-view>'
        }
      }
    });
  }

  angular.module('app.profile').config(route);
}());
