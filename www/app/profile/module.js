(function _AppProfileModule_() {
  'use strict';

  var modules = [
    'app.profile.mine'
  ];

  angular.module('app.profile', modules);
}());
