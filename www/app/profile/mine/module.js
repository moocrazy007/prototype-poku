(function _AppProfileMineModule_() {
  'use strict';

  var modules = [];

  angular.module('app.profile.mine', modules);
}());
