(function _AppProfileMineController_() {
  'use strict';

  function ProfileMineController() {
    var vm = this;

    vm.content = 'My Profile Content';
  }

  angular.module('app.profile').controller('ProfileMineController', ProfileMineController);
}());
