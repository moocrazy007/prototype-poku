(function _AppProfileMineRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.profile.mine', {
      url: '/mine',
      views: {
        'menuContent@app.profile': {
          templateUrl: 'app/profile/mine/index.html',
          controller: 'ProfileMineController',
          controllerAs: 'profile'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/profile/mine/controller.js');
        }
      }
    });
  }

  angular.module('app.profile.mine').config(route);
}());
