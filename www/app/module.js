(function _AppModule_() {
  'use strict';

  var modules = [
    'ionic',
    'oc.lazyLoad',
    'app.admedika',
    'app.auth',
    'app.home',
    'app.profile',
    'app.services',
    'app.setting',
    'app.support',
    'app.user'
  ];

  angular.module('app', modules);
}());
