(function _AppRun_() {
  'use strict';

  function run($ionicPlatform, $window) {
    $ionicPlatform.ready(function onReady() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if ($window.cordova && $window.cordova.plugins.Keyboard) {
        $window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        $window.cordova.plugins.Keyboard.disableScroll(true);
      }
      // org.apache.cordova.statusbar required
      if ($window.StatusBar) {
        $window.StatusBar.styleDefault();
      }
    });
  }

  angular.module('app').run(run);
}());
