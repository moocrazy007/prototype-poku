(function _AppConfig_() {
  'use strict';

  function graphql(graphqlProvider) {
    graphqlProvider.set({
      url: 'http://localhost:4320/graphql'
    });
  }

  function ionicConfig($ionicConfigProvider) {
    $ionicConfigProvider.backButton.text('');
    $ionicConfigProvider.backButton.previousTitleText(false);
  }

  function log($logProvider) {
    $logProvider.debugEnabled(true);
  }

  angular.module('app')
    .config(graphql)
    .config(ionicConfig)
    .config(log);
}());
