(function _AppSettingModule_() {
  'use strict';

  var modules = [];

  angular.module('app.setting', modules);
}());
