(function _AppSettingController_() {
  'use strict';

  function SettingController() {
    var vm = this;

    vm.content = 'Setting Menu Content';
  }

  angular.module('app.setting').controller('SettingController', SettingController);
}());
