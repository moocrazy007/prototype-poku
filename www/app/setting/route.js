(function _AppSettingRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.setting', {
      url: '/setting',
      views: {
        'menuContent@app': {
          templateUrl: 'app/setting/index.html',
          controller: 'SettingController',
          controllerAs: 'setting'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/setting/controller.js');
        }
      }
    });
  }

  angular.module('app.setting').config(route);
}());
