(function _AppRoute_() {
  'use strict';

  function route($stateProvider, $urlRouterProvider) {
    $stateProvider.state('app', {
      url: '',
      abstract: true,
      views: {
        '': {
          templateUrl: 'app/index.html',
          controller: 'AppController',
          controllerAs: 'app'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/controller.js');
        }
      }
    });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/');
  }

  angular.module('app').config(route);
}());
