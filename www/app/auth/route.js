(function _AppAuthRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.auth', {
      url: '/auth',
      abstract: true,
      views: {
        'menuContent@app': {
          template: '<ion-nav-view name="menuContent"></ion-nav-view>'
        }
      }
    });
  }

  angular.module('app.auth').config(route);
}());
