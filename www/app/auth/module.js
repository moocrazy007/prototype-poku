(function _AppAuthModule_() {
  'use strict';

  var modules = [
    'app.auth.activate',
    'app.auth.activate-success',
    'app.auth.change-password',
    'app.auth.forget-password',
    'app.auth.forget-password-success',
    'app.auth.login',
    'app.auth.register'
  ];

  angular.module('app.auth', modules);
}());
