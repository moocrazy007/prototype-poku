(function _AppAuthChangePasswordController_() {
  'use strict';

  function AuthChangePasswordController() {
    var vm = this;

    vm.content = 'Change Password Content';
  }

  angular.module('app.auth.change-password').controller('AuthChangePasswordController', AuthChangePasswordController);
}());
