(function _AppAuthChangePasswordModule_() {
  'use strict';

  var modules = [];

  angular.module('app.auth.change-password', modules);
}());
