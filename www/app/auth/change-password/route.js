(function _AppAuthChangePasswordRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.auth.change-password', {
      url: '/change-password',
      views: {
        'menuContent@app.auth': {
          templateUrl: 'app/auth/change-password/index.html',
          controller: 'AuthChangePasswordController',
          controllerAs: 'auth'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/auth/change-password/controller.js');
        }
      }
    });
  }

  angular.module('app.auth.change-password').config(route);
}());
