(function _AppAuthActivateController_() {
  'use strict';

  function AuthActivateController() {
    var vm = this;

    vm.content = 'Activate Account Content';
  }

  angular.module('app.auth.activate').controller('AuthActivateController', AuthActivateController);
}());
