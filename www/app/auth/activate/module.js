(function _AppAuthActivateModule_() {
  'use strict';

  var modules = [];

  angular.module('app.auth.activate', modules);
}());
