(function _AppAuthRegisterRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.auth.register', {
      url: '/register',
      views: {
        'menuContent@app.auth': {
          templateUrl: 'app/auth/register/index.html',
          controller: 'AuthRegisterController',
          controllerAs: 'auth'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/auth/register/controller.js');
        }
      }
    });
  }

  angular.module('app.auth.register').config(route);
}());
