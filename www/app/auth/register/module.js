(function _AppAuthRegisterModule_() {
  'use strict';

  var modules = [];

  angular.module('app.auth.register', modules);
}());
