(function _AppAuthRegisterController_() {
  'use strict';

  function AuthRegisterController() {
    var vm = this;

    vm.content = 'Register Content';
  }

  angular.module('app.auth.register').controller('AuthRegisterController', AuthRegisterController);
}());
