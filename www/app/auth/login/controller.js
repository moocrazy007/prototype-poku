(function _AppAuthLoginController_() {
  'use strict';

  function AuthLoginController() {
    var vm = this;

    vm.content = 'Login Content';
  }

  angular.module('app.auth.login').controller('AuthLoginController', AuthLoginController);
}());
