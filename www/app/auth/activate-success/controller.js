(function _AppAuthActivateSuccessController_() {
  'use strict';

  function AuthActivateSuccessController() {
    var vm = this;

    vm.content = 'Activate Account Success Content';
  }

  angular.module('app.auth.activate-success').controller('AuthActivateSuccessController', AuthActivateSuccessController);
}());
