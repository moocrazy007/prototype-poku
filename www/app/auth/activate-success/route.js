(function _AppAuthActivateSuccessRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.auth.activate-success', {
      url: '/activate-success',
      views: {
        'menuContent@app.auth': {
          templateUrl: 'app/auth/activate-success/index.html',
          controller: 'AuthActivateSuccessController',
          controllerAs: 'auth'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/auth/activate-success/controller.js');
        }
      }
    });
  }

  angular.module('app.auth.activate-success').config(route);
}());
