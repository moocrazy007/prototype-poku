(function _AppAuthActivateSuccessModule_() {
  'use strict';

  var modules = [];

  angular.module('app.auth.activate-success', modules);
}());
