(function _AppHomeRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.home', {
      url: '/',
      views: {
        'menuContent@app': {
          templateUrl: 'app/home/index.html',
          controller: 'HomeController',
          controllerAs: 'home'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/home/controller.js');
        }
      }
    });
  }

  angular.module('app.home').config(route);
}());
