(function _AppHomeController_() {
  'use strict';

  function HomeController($log, graphql) {
    var vm = this;

    vm.content = 'Home Content';

    graphql('query { users { _id, name, email }  }')
      .then(function ok(res) {
        $log.info(res);
      })
      .catch(function error(err) {
        $log.error(err);
      });
  }

  angular.module('app.home').controller('HomeController', HomeController);
}());
