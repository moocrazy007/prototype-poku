(function _AppHomeModule_() {
  'use strict';

  var modules = [];

  angular.module('app.home', modules);
}());
