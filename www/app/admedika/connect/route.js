(function _AppAdmedikaConnectRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.admedika.connect', {
      url: '/connect',
      views: {
        'menuContent@app.admedika': {
          templateUrl: 'app/admedika/connect/index.html',
          controller: 'AdmedikaConnectController',
          controllerAs: 'admedika'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/admedika/connect/controller.js');
        }
      }
    });
  }

  angular.module('app.admedika.connect').config(route);
}());
