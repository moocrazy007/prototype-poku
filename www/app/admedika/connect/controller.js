(function _AppAdmedikaConnectController_() {
  'use strict';

  function AdmedikaConnectController() {
    var vm = this;

    vm.content = 'Connect My AdMedika Card Content';
  }

  angular.module('app.admedika.connect').controller('AdmedikaConnectController', AdmedikaConnectController);
}());
