(function _AppAdmedikaConnectModule_() {
  'use strict';

  var modules = [];

  angular.module('app.admedika.connect', modules);
}());
