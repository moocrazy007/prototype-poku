(function _AppAdmedikaRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.admedika', {
      url: '/admedika',
      abstract: true,
      views: {
        'menuContent@app': {
          template: '<ion-nav-view name="menuContent"></ion-nav-view>'
        }
      }
    });
  }

  angular.module('app.admedika').config(route);
}());
