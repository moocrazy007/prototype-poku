(function _AppAdmedikaModule_() {
  'use strict';

  var modules = [
    'app.admedika.benefit',
    'app.admedika.claim',
    'app.admedika.connect',
    'app.admedika.member',
    'app.admedika.provider',
    'app.admedika.show',
    'app.admedika.status'
  ];

  angular.module('app.admedika', modules);
}());
