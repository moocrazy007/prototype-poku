(function _AppAdmedikaStatusRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.admedika.status', {
      url: '/status',
      views: {
        'menuContent@app.admedika': {
          templateUrl: 'app/admedika/status/index.html',
          controller: 'AdmedikaStatusController',
          controllerAs: 'admedika'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/admedika/status/controller.js');
        }
      }
    });
  }

  angular.module('app.admedika.status').config(route);
}());
