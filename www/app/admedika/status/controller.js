(function _AppAdmedikaStatusController_() {
  'use strict';

  function AdmedikaStatusController() {
    var vm = this;

    vm.content = 'My Status Content';
  }

  angular.module('app.admedika.status').controller('AdmedikaStatusController', AdmedikaStatusController);
}());
