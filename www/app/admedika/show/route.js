(function _AppAdmedikaShowRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.admedika.show', {
      url: '/show',
      views: {
        'menuContent@app.admedika': {
          templateUrl: 'app/admedika/show/index.html',
          controller: 'AdmedikaShowController',
          controllerAs: 'admedika'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/admedika/show/controller.js');
        }
      }
    });
  }

  angular.module('app.admedika.show').config(route);
}());
