(function _AppAdmedikaShowController_() {
  'use strict';

  function AdmedikaShowController() {
    var vm = this;

    vm.content = 'Show My Digital Card Content';
  }

  angular.module('app.admedika.show').controller('AdmedikaShowController', AdmedikaShowController);
}());
