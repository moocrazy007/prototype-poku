(function _AppAdmedikaClaimController_() {
  'use strict';

  function AdmedikaClaimController() {
    var vm = this;

    vm.content = 'Claim Content';
  }

  angular.module('app.admedika.claim').controller('AdmedikaClaimController', AdmedikaClaimController);
}());
