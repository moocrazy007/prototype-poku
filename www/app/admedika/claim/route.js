(function _AppAdmedikaClaimRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.admedika.claim', {
      url: '/claim',
      views: {
        'menuContent@app.admedika': {
          templateUrl: 'app/admedika/claim/index.html',
          controller: 'AdmedikaClaimController',
          controllerAs: 'admedika'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/admedika/claim/controller.js');
        }
      }
    });
  }

  angular.module('app.admedika.claim').config(route);
}());
