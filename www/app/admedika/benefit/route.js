(function _AppAdmedikaBenefitRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.admedika.benefit', {
      url: '/benefit',
      views: {
        'menuContent@app.admedika': {
          templateUrl: 'app/admedika/benefit/index.html',
          controller: 'AdmedikaBenefitController',
          controllerAs: 'admedika'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/admedika/benefit/controller.js');
        }
      }
    });
  }

  angular.module('app.admedika.benefit').config(route);
}());
