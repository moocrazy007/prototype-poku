(function _AppAdmedikaBenefitController_() {
  'use strict';

  function AdmedikaBenefitController() {
    var vm = this;

    vm.content = 'Benefit Content';
  }

  angular.module('app.admedika.benefit').controller('AdmedikaBenefitController', AdmedikaBenefitController);
}());
