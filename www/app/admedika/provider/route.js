(function _AppAdmedikaProviderRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.admedika.provider', {
      url: '/provider',
      views: {
        'menuContent@app.admedika': {
          templateUrl: 'app/admedika/provider/index.html',
          controller: 'AdmedikaProviderController',
          controllerAs: 'admedika'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/admedika/provider/controller.js');
        }
      }
    });
  }

  angular.module('app.admedika.provider').config(route);
}());
