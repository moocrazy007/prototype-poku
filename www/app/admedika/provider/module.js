(function _AppAdmedikaProviderModule_() {
  'use strict';

  var modules = [];

  angular.module('app.admedika.provider', modules);
}());
