(function _AppAdmedikaProviderController_() {
  'use strict';

  function AdmedikaProviderController() {
    var vm = this;

    vm.content = 'Provider Content';
  }

  angular.module('app.admedika.provider').controller('AdmedikaProviderController', AdmedikaProviderController);
}());
