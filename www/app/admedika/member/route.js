(function _AppAdmedikaMemberRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.admedika.member', {
      url: '/member',
      views: {
        'menuContent@app.admedika': {
          templateUrl: 'app/admedika/member/index.html',
          controller: 'AdmedikaMemberController',
          controllerAs: 'admedika'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/admedika/member/controller.js');
        }
      }
    });
  }

  angular.module('app.admedika.member').config(route);
}());
