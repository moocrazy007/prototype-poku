(function _AppAdmedikaMemberController_() {
  'use strict';

  function AdmedikaMemberController() {
    var vm = this;

    vm.content = 'Member Info Content';
  }

  angular.module('app.admedika.member').controller('AdmedikaMemberController', AdmedikaMemberController);
}());
