(function _AppSupportFaqRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.support.faq', {
      url: '/faq',
      views: {
        'menuContent@app.support': {
          templateUrl: 'app/support/faq/index.html',
          controller: 'SupportFaqController',
          controllerAs: 'support'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/support/faq/controller.js');
        }
      }
    });
  }

  angular.module('app.support.faq').config(route);
}());
