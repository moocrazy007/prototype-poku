(function _AppSupportFaqController_() {
  'use strict';

  function SupportFaqController() {
    var vm = this;

    vm.content = 'FAQ Content';
  }

  angular.module('app.support.faq').controller('SupportFaqController', SupportFaqController);
}());
