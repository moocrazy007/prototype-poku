(function _AppSupportFaqModule_() {
  'use strict';

  var modules = [];

  angular.module('app.support.faq', modules);
}());
