(function _AppSupportAboutController_() {
  'use strict';

  function SupportAboutController() {
    var vm = this;

    vm.content = 'About Content';
  }

  angular.module('app.support.about').controller('SupportAboutController', SupportAboutController);
}());
