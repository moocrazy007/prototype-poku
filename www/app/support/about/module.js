(function _AppSupportAboutModule_() {
  'use strict';

  var modules = [];

  angular.module('app.support.about', modules);
}());
