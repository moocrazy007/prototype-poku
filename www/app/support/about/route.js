(function _AppSupportAboutRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.support.about', {
      url: '/about',
      views: {
        'menuContent@app.support': {
          templateUrl: 'app/support/about/index.html',
          controller: 'SupportAboutController',
          controllerAs: 'support'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/support/about/controller.js');
        }
      }
    });
  }

  angular.module('app.support.about').config(route);
}());
