(function _AppSupportRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.support', {
      url: '/support',
      abstract: true,
      views: {
        'menuContent@app': {
          template: '<ion-nav-view name="menuContent"></ion-nav-view>'
        }
      }
    });
  }

  angular.module('app.support').config(route);
}());
