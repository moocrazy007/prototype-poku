(function _AppSupportContactUsRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.support.contact-us', {
      url: '/contact-us',
      views: {
        'menuContent@app.support': {
          templateUrl: 'app/support/contact-us/index.html',
          controller: 'SupportContactUsController',
          controllerAs: 'support'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/support/contact-us/controller.js');
        }
      }
    });
  }

  angular.module('app.support.contact-us').config(route);
}());
