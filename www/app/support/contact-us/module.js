(function _AppSupportContactUsModule_() {
  'use strict';

  var modules = [];

  angular.module('app.support.contact-us', modules);
}());
