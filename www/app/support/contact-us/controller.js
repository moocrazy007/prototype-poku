(function _AppSupportContactUsController_() {
  'use strict';

  function SupportContactUsController() {
    var vm = this;

    vm.content = 'Contact Us Content';
  }

  angular.module('app.support.contact-us').controller('SupportContactUsController', SupportContactUsController);
}());
