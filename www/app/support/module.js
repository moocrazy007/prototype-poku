(function _AppSupportModule_() {
  'use strict';

  var modules = [
    'app.support.about',
    'app.support.contact-us',
    'app.support.faq',
    'app.support.terms-condition'
  ];

  angular.module('app.support', modules);
}());
