(function _AppSupportTermsConditionRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.support.terms-condition', {
      url: '/terms-condition',
      views: {
        'menuContent@app.support': {
          templateUrl: 'app/support/terms-condition/index.html',
          controller: 'SupportTermsConditionController',
          controllerAs: 'support'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/support/terms-condition/controller.js');
        }
      }
    });
  }

  angular.module('app.support.terms-condition').config(route);
}());
