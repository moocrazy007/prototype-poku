(function _AppSupportTermsConditionController_() {
  'use strict';

  function SupportTermsConditionController() {
    var vm = this;

    vm.content = 'Terms and Condition Content';
  }

  angular.module('app.support.terms-condition').controller('SupportTermsConditionController', SupportTermsConditionController);
}());
