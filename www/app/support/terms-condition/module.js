(function _AppSupportTermsConditionModule_() {
  'use strict';

  var modules = [];

  angular.module('app.support.terms-condition', modules);
}());
