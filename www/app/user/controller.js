(function _AppUserController_() {
  'use strict';

  function UserController($log, graphql) {
    var vm = this;

    graphql('query { users { _id, name, email } }')
      .then(function success(response) {
        vm.list = response;
      })
      .catch(function failed(error) {
        $log.error(error);
      });
  }

  angular.module('app.user').controller('UserController', UserController);
}());
