(function _AppUserModule_() {
  'use strict';

  var modules = [];

  angular.module('app.user', modules);
}());
