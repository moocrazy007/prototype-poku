(function _AppUserRoute_() {
  'use strict';

  function route($stateProvider) {
    $stateProvider.state('app.user', {
      url: '/user',
      views: {
        'menuContent@app': {
          templateUrl: 'app/user/index.html',
          controller: 'UserController',
          controllerAs: 'user'
        }
      },
      resolve: {
        controller: function getController($ocLazyLoad) {
          return $ocLazyLoad.load('app/user/controller.js');
        }
      }
    });
  }

  angular.module('app.user').config(route);
}());
